Debate Strategy Sheets
======================
This repository compiles plans and frontlines for the Southlake Carroll Debate team.

The repository is structured as follows:

* There is one folder for every team in the "Schools" directory.
* Each team's folder contains a document with the title "Plan - [TOPIC]". This is the team's plan. The plans topic is represented by the [TOPIC].
* There is a folder for each case in the "Cases" directory.
* Each case folder has a file called "frontline.docx" with that team's case.
* The Frontline contains a response and extensions for every block.

After every round (where you are Affirmative), UPDATE THE REPOSITORY!

Create new frontlines in new git Branches. When you create a new branch for a frontline, name it "[TEAM NAME] - Frontline".

Frontlines should (ideally) give a team everything they need for the whole round (as Negative). Do NOT merge a new frontline's branch unless it is completely done!

If you need a Frontline and its not done, see if there's an in-progress version by listing all of the frontlines currently in development. You can do this with the command "git branch".
